import os
import sys
import bottle
from bottle import default_app, route, get, post, request,static_file, error, run, route, abort
bottle.BaseRequest.MEMFILE_MAX = 4096 * 2048 # (or whatever you want)
from pylatex import  Document, Section, Subsection, Tabular, Math, TikZ, Axis, Plot, Figure, Package, Matrix, Command
from pylatex.utils import italic
import collections
from jinja2.loaders import FileSystemLoader
from latex.jinja2 import make_env
from bottle import response
from latex import build_pdf
import numpy as np
from io import BytesIO
import math
@route('/')
def hello_world():
    return '''
        <html>
        <head>
            <title>Text calculations</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        </head>
        <body style="background-color:#ecf0f1;width:60vw;">
        <h1 class="col-sm-offset-2">Text calculations <br /><small>Type text below and get it's info in PDF or TEX file</small></h1>
        <form class="form-horizontal" style="padding-top:10vh;"action="/act" method="post">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Input text:</label>
            <div class="col-sm-10">
                <textarea style="width:50vw;height:25vh" class="form-control" name="text" type="text" ></textarea>
            </div>
        </div>

        <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <div class="checkbox">
            <label>
              <input type="checkbox" id="down" name="down" value="true"> Download file
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" id="tex" name="tex" value="true"> Get TEX-source
            </label>
          </div>
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Proceed</button>
        </div>
        </div>
        </form>

        </body>
        </html>
    '''
@error(500)
def custom500(error):
    return '500 ERROR ACCURED: <br /><pre>'+str(error).replace("\\n","\n")+"</pre>"

@post ('/act')
def textan():

    text = request.forms.get('text')
    if len(text)<1:
        return '<h1>Text is too short for calculation'
    if not(request.forms.get('tex')):
        response.headers['Content-Type'] = 'application/pdf; charset=WINDOWS-1251'
    if request.forms.get('down'):
        if request.forms.get('tex'):
            response.headers['Content-Disposition'] = 'attachment; filename="output.tex"'
        else:
            response.headers['Content-Disposition'] = 'attachment; filename="output.pdf"'
    #text =text.rstrip()
    prob = [ float(text.count(cq)) / len(text) for cq in dict.fromkeys(list(text)) ]
    entropy = - sum([ p * math.log(p) / math.log(2.0) for p in prob ])

    count = {}
    letters = {}
    for letter in text:
      if letter not in count:
            letters[letter] = letter
            count[letter] = 1
      else:
        count[letter] += 1
    lettersSt = "("
    lettLabs = ""
    maxAmount = 0
    mapping = {",":"{,}","*":"\\*","\n":"nl","\\":"bksl","\r":"rb",  "$":"dolr",  ":":"\\:",  " ":"sp"," ":"sp",   "(":"{(}", ")":"{)}","%":"\\%","^":"\\^","{ ":"\\{","_":"\\_","¶":"\\P","|":"\\textbar ",">":"\\textgreater ","-":"\\textendash ","™":"\\texttrademark ","¡":"\\textexclamdown ","£":"\\pounds ","#":"\\#","&":"\\&","} ":"\\}","§":"\\S","†":"\\dag ","<":"\\textless ","—":"\\textemdash","®":"\\textregistered","¿":"\\textquestiondown","©":"\\copyright"}
    for x in count:
        for (a,b) in mapping.items():
          letters[x] = letters[x].replace(a,b)
        if letters[x] != "rb":
            lettersSt += ""+letters[x]+","+str(count[x])+") ("
            lettLabs +=letters[x]+","
            if maxAmount<count[x]: maxAmount=count[x]
    lettersSt += ")"
    lettLabs =lettLabs[:-1]
    lettersSt = lettersSt.replace(" ()","")
    lettersSt = lettersSt.replace("()","")
    plotHeight = (6+maxAmount*0.01)
    if plotHeight>22:
        plotHeight=22
    mapping2 = {"%":"\\%","$":"\\$","^":"\\^","{ ":"\\{","_":"\\_","¶":"\\P","|":"\\textbar ",">":"\\textgreater ","-":"\\textendash ","™":"\\texttrademark ","¡":"\\textexclamdown ","£":"\\pounds ","#":"\\#","&":"\\&","} ":"\\}","§":"\\S","†":"\\dag ","<":"\\textless ","—":"\\textemdash","®":"\\textregistered","¿":"\\textquestiondown","©":"\\copyright"}
    latText = list(text.replace("\\","(~)"))
    for x in range(0, len(text), 1):
        for (a,b) in mapping2.items():
          latText[x] = latText[x].replace(a,b)
    min_latex = ("""
    \\documentclass[2pt,a4paper]{article}
    \\usepackage{tikz}
    \\usepackage[a4paper, margin=0.2in]{geometry}
    \\usetikzlibrary{decorations.text}
    \\usepackage{pgfplots}
    \\usetikzlibrary{fit}
    \\pgfplotsset{compat=1.9}

    \\begin{document}
    \\tableofcontents
    \\section{Input}
    %s
    \\section{Results}
    \\begin{flushleft}
    \\begin{tabular}{|c|c|}
        \\hline
            Text length &%i\\\\
        \\hline
            Entropy  &%f\\\\
        \\hline
    \\end{tabular}
    \\end{flushleft}
    See Fig.~\\ref{fig:freq} for details

    \\begin{figure}[!h]
    %%\\flushleft
    \\centering
    \\begin{tikzpicture}
    %%1\\textwidth
    \\begin{axis}[symbolic x coords={%s},xtick=data, width=%fcm, height=%dcm, bar width=0.05cm,nodes near coords, nodes near coords align={vertical}, ymin=0]
        \\addplot [ybar,fill=blue, enlarge y limits=0]coordinates {%s};
    \\end{axis}
    \\end{tikzpicture}
    \\caption{Frequency of each character}
    \\label{fig:freq}
    \\end{figure}

    \\end{document}

     """ % ("".join(latText).replace("(~)","\\textbackslash"), len(text), entropy+0,  lettLabs, (3 + len(lettLabs)*0.1), plotHeight ,lettersSt))
    if request.forms.get('tex'):
        return ("<pre>"+min_latex+"</pre>")
    else:
        return bytes(build_pdf(min_latex))

application = default_app()

